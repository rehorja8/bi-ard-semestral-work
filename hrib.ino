// HRIB - Honzuv Rehoruv Interaktivni Brainf*ck
// Made by: Jan Řehoř 2023 as a semestral project for BI-ARD

#include <Esplora.h>
#include <TFT.h>            // Arduino LCD library
#include <SPI.h>

// Define EsploraTFT as 'screen' for better code readability
#define screen EsploraTFT

// Size of stack - used for brackets detection
#define STACK_SIZE 128

// Threshold for detecting joystick tilt
#define JOY_THRESHOLD 40

// Default colors used in program
#define BLACK 0,0,0
#define WHITE 255,255,255
#define GREEN 0,255,0
#define RED 255,0,0

// Section with dimensions and parameters of UI elements
#define CHAR_HEIGHT 9
#define CHAR_WIDTH 7
// Text representing inputed code
#define CODE_X 2
#define CODE_Y 33
#define CODE_COLOR_BG BLACK
#define CODE_COLOR_FG WHITE
#define CODE_LINE_LEN 22
#define CODE_LEN 153
// List of available characters for user's input
#define WRITER_FG WHITE
#define WRITER_BG BLACK
#define WRITER_MARGIN 3
#define WRITER_HEIGHT (CHAR_HEIGHT + 2 * WRITER_MARGIN)
#define WRITER_Y (screen.height() - WRITER_HEIGHT) - 1
#define WRITER_MARGIN 1
// Text representing input for executed code
#define INPUT_LEN 19
#define INPUT_LINE_LEN 10
#define INPUT_X 0
#define INPUT_Y (WRITER_Y - (2 * CHAR_HEIGHT) - 1)
// Text representing output of executed code
#define OUTPUT_LEN 20
#define OUTPUT_LINE_LEN 10
#define OUTPUT_X (INPUT_X + CHAR_WIDTH * (INPUT_LINE_LEN + 2))
#define OUTPUT_Y INPUT_Y
// Text showing hints/errors/state of the editor
#define MESSAGE_X 0
#define MESSAGE_Y 2
#define MESSAGE_MAX_LEN 24
// Text representing memory of the program
#define MEMORY_NUM_OF_BYTES 12
#define MEMORY_X 0
#define MEMORY_Y (MESSAGE_Y + CHAR_HEIGHT + 2)

// Sets of characters that user can write
const char SYMBOLS_DIGITS[]     = "0123456789 !+-*/=()";
const char SYMBOLS_ALPHABET_1[] = "ABCDEFGHIJKLM";
const char SYMBOLS_ALPHABET_2[] = "NOPQRSTUVWXYZ";
const char SYMBOLS_CODE[]       = "+-<>[],.";

// Struct used for better management of colors (storing and setting)
struct CColor
{
  byte m_R;
  byte m_G;
  byte m_B;
  CColor(byte r, byte g, byte b) : m_R(r), m_G(g), m_B(b)
  {}

  // Sets stroke and background to this instance of CColor
  void set() const
  {
    screen.fill(m_R, m_G, m_B);
    screen.stroke(m_R, m_G, m_B);
  }
};

// Struct representing button on Esplora
// with quality of life improvements
struct CButton
{
    // Enum with options of buttons on esplora
    enum EButton
    {
      UP, DOWN, RIGHT, LEFT, JOY, JOY_LEFT, JOY_RIGHT
    };

    CButton(EButton button)
    {
      m_Button = button;
      m_PrevState = 0;
      m_CurrentState = 0;
    }

    // Updates button's internal state
    // Methods isHold() and isPressed() do not change their
    // return values between call of this update()
    void update()
    {
      m_PrevState = m_CurrentState;
      m_CurrentState = getState();
    }

    // Returns true if the button is pressed
    bool isHold()
    {
      return m_CurrentState;
    }

    // Returns true if the button is pressed and was not pressed before
    // (detects rising edge)
    bool isPressed()
    {
      return m_CurrentState && !m_PrevState;
    }

    // Returns true and false in short periods of time when button is pressed down
    bool isRepeated()
    {
      static bool hasBeenRepeated = false;
      if (!isHold())
        return false;

      if (!hasBeenRepeated && millis() % (2 * REPEAT_DELAY) < REPEAT_DELAY)
      {
        hasBeenRepeated = true;
        return true;
      }

      if (millis() % (2 * REPEAT_DELAY) > REPEAT_DELAY)
        hasBeenRepeated = false;

      return false;
    }

  private:
    const int REPEAT_DELAY = 80;
    bool m_PrevState;
    bool m_CurrentState;
    EButton m_Button;

    // Return button's state on the Esplora
    bool getState()
    {
      switch (m_Button)
      {
        case UP:
          return !Esplora.readButton(SWITCH_UP);
        case DOWN:
          return !Esplora.readButton(SWITCH_DOWN);
        case RIGHT:
          return !Esplora.readButton(SWITCH_RIGHT);
        case LEFT:
          return !Esplora.readButton(SWITCH_LEFT);
        case JOY:
          return !Esplora.readJoystickButton();
        case JOY_RIGHT:
          return (Esplora.readJoystickX() > JOY_THRESHOLD);
        case JOY_LEFT:
          return (Esplora.readJoystickX() < -JOY_THRESHOLD);
        default:
          return 0;
      }
    }
};

// UI element for displaying and storing text
struct CText
{
    char* m_Text;         // Stored string
    uint16_t m_MaxLen;    // Maximum length of string
    uint16_t m_LineLen;   // Lenght of one line
    uint16_t m_AppendPos; // Index at which new character is supposed to be appended, see 'void append(byte c)'
    uint16_t m_ReadPos;   // Index from where the character in m_Text should be read, see 'byte read()'
    CColor m_Fg;          // Foreground color
    CColor m_Bg;          // Background color
    byte m_X;             // X coordinate of this UI element
    byte m_Y;             // Y coordinate of this UI element


    CText(char* text, uint16_t maxLen, uint16_t lineLen, byte x, byte y)
      : m_Text(text), m_MaxLen(maxLen), m_LineLen(lineLen), m_AppendPos(0), m_ReadPos(0), m_X(x), m_Y(y), m_Fg(CODE_COLOR_FG), m_Bg(CODE_COLOR_BG)
    {}

    CText(char* text, uint16_t maxLen, uint16_t lineLen, byte x, byte y, CColor fg, CColor bg)
      : m_Text(text), m_MaxLen(maxLen), m_LineLen(lineLen), m_AppendPos(0), m_ReadPos(0), m_X(x), m_Y(y), m_Fg(fg), m_Bg(bg)
    {}

    // Renders text stored in m_Text from 'updateFromId'
    void updateText(uint16_t updateFromId = 0)
    {
      for (uint16_t i = updateFromId; i <= m_MaxLen; i++)
      {
        putCharAt(m_Text[i], i);
      }
    }

    // Sets text's color
    void setColor(bool inverted)
    {
      if (!inverted)
      {
        m_Fg.set();
      }
      else
      {
        m_Bg.set();
      }
    }

    // Sets m_AppendPos to zero
    void resetAppend()
    {
      m_AppendPos = 0;
    }

    // Appends a byte at m_AppendPos
    // If 'asNumber' is set, the byte is
    // printed not as it's ASCII value but as a number
    void append(byte c, bool asNumber = false, bool reset = false)
    {
      if (reset) resetAppend();
      if (!asNumber)
      {
        putCharAt(c, m_AppendPos++);
        m_AppendPos %= m_MaxLen;
        return;
      }

      // Convert number to text before appending
      char buffer[4];
      snprintf(buffer, 4, "%d", c);
      int len = strlen(buffer);
      for (int i = 0; i < len; i++)
      {
        append(buffer[i], false);
      }
    }

    // Sets m_ReadPos to zero
    void resetRead()
    {
      m_ReadPos = 0;
    }

    // Returns byte's value in m_Test at m_ReadPos
    // If 'asNmber' is set and read byte is a digit,
    // returned value is a number and not it's ASCII representation
    byte read(bool asNumber = false)
    {
      int len = strlen(m_Text);
      if (len == 0 || len < m_ReadPos) return 0;

      byte result = m_Text[m_ReadPos++];
      if (asNumber && result >= '0' && result <= '9') result -= '0';
      return result;
    }

    uint16_t getLength()
    {
      return strlen(m_Text);
    }

    // Prints a character at postion according to UI element's
    // dimensions a position
    void putCharAt(char c, uint16_t id, bool inverted = false)
    {
      byte x = getXAt(id);
      byte y = getYAt(id);

      setColor(!inverted);
      screen.rect(x, y, CHAR_WIDTH, CHAR_HEIGHT);

      setColor(inverted);
      char tmp[2] = {c, '\0'};
      screen.text(tmp , x + 1, y + 1);
    }

    // Highlights a character at index by inverting it's foreground and background color
    void highlightAt(uint16_t id)
    {
      putCharAt(m_Text[id], id, 1);
    }

    // Un-highlights a character at index
    void normalAt(uint16_t id)
    {
      putCharAt(m_Text[id], id, 0);
    }

    // Clears all rendered text
    void reset()
    {
      for (int i = 0; i < m_MaxLen; i++)
      {
        normalAt(i);
        putCharAt(' ', i);
      }
    }
  private:

    // Returns coordinates of a supposed character based on CText position
    // based on character's index
    byte getXAt(uint16_t id)
    {
      return m_X + CHAR_WIDTH * (id % m_LineLen);
    }
    byte getYAt(uint16_t id)
    {
      return m_Y + CHAR_HEIGHT * (id / m_LineLen);
    }

};

// Struct for user input
// Manipulates with an instance of CText (inputs characters, deletes them, ...)
// CWriter shows also a selection of available characters, that can be inputed
struct CWriter
{
    CWriter(CButton* enter, CButton* backspace, CButton* left, CButton* right, char* symbols, uint16_t symbolsLen, CText* textObject)
      : m_Enter(enter), m_Backspace(backspace), m_Left(left), m_Right(right),
        m_SymbolSet(symbols),
        m_SymbolSetLenght(symbolsLen),
        m_Text(textObject),
        m_CursorPos(0),
        m_SymbolsText(symbols, symbolsLen, 20, 0, WRITER_Y + 1)
    {}

    // Sets up this instance of CWriter and renders selection of available characters
    void init()
    {
      clearBackground();
      drawLines();
      m_Text->highlightAt(m_CursorPos);
      m_SymbolsText.updateText();
    }

    // Updates a CText instance according to button states
    // Returns true if CText got modified
    bool update()
    {
      drawSet();
      updateCursor();
      return updateText();
    }

    // Renders selection of available characters
    void drawSet()
    {
      static char prevSelectedSymbol = 0;
      char selectedSymbol = getSelectedSymbol();

      if (selectedSymbol != prevSelectedSymbol)
      {
        for (byte i = 0; i < m_SymbolsText.m_MaxLen; i++)
        {
          if (m_SymbolsText.m_Text[i] == selectedSymbol)
            m_SymbolsText.highlightAt(i);
          else if (m_SymbolsText.m_Text[i] == prevSelectedSymbol)
            m_SymbolsText.normalAt(i);
        }
      }

      prevSelectedSymbol = selectedSymbol;
    }

    // Hides selection of characters
    // and cursor in a CText instance
    void defocus()
    {
      m_Text->normalAt(m_CursorPos);
      clearBackground();
    }

    // Renders available text
    // and cursor in a CText instance
    void focus()
    {
      init();
    }

    // Changes selection of available characters that
    // can be inputed with this instance of CWriter
    void changeSymbolSet(char* newSet, byte newSetLen)
    {
      m_SymbolSet = newSet;
      m_SymbolSetLenght = newSetLen;
      m_SymbolsText = CText(m_SymbolSet, m_SymbolSetLenght, 20, 0, WRITER_Y + 1);

      clearBackground();

      m_SymbolsText.updateText();
      m_SymbolsText.highlightAt(getSelectedId());
      drawLines();
    }
    
  private:
  
    // Hides selection of characters
    void clearBackground()
    {
      screen.fill(WRITER_BG);
      screen.stroke(WRITER_BG);
      screen.rect(0, WRITER_Y, screen.width(), WRITER_HEIGHT);
    }

    // Updates cursor position according to button states
    void updateCursor()
    {
      static uint16_t m_PrevCursorPos = m_CursorPos;
      if (m_Left->isRepeated() && m_CursorPos < m_Text->getLength())
        m_CursorPos++;

      if (m_Right->isRepeated() && m_CursorPos > 0)
        m_CursorPos--;

      if (m_PrevCursorPos != m_CursorPos)
      {
        m_Text->normalAt(m_PrevCursorPos);
        m_Text->highlightAt(m_CursorPos);
        m_PrevCursorPos = m_CursorPos;
      }
    }

    // Updates an instace of CText according to button states
    // Returns true if CText got modified
    bool updateText()
    {
      if (m_CursorPos < m_Text->m_MaxLen && m_Enter->isPressed() && m_Text->getLength() < m_Text->m_MaxLen)
      {
        char symbol = getSelectedSymbol();
        //
        memmove(m_Text->m_Text + m_CursorPos + 1, m_Text->m_Text + m_CursorPos, m_Text->getLength() - m_CursorPos);
        m_Text->m_Text[m_CursorPos] = symbol;
        m_Text->updateText(m_CursorPos);
        //
        m_CursorPos++;
        m_Text->highlightAt(m_CursorPos);
        return true;
      }
      else if (m_CursorPos > 0 && m_Backspace->isPressed())
      {
        m_CursorPos--;
        memmove(m_Text->m_Text + m_CursorPos, m_Text->m_Text + m_CursorPos + 1, m_Text->getLength() - m_CursorPos);
        m_Text->updateText(m_CursorPos);
        m_Text->highlightAt(m_CursorPos);
        return true;
      }
      return false;
    }


    // Draws lines around selection
    void drawLines()
    {
      screen.fill(WRITER_FG);
      screen.stroke(WRITER_FG);
      screen.line(0, WRITER_Y, screen.width(), WRITER_Y);
      screen.line(0, WRITER_Y + CHAR_HEIGHT + 1 , screen.width(), WRITER_Y + CHAR_HEIGHT + 1);
    }

    // Returns index in selection array determined using Esplora's slider
    uint16_t getSelectedId()
    {
      return map(Esplora.readSlider(), 1024, 0, 0, m_SymbolSetLenght - 1);
    }

    // Returns selected symbol in character selection
    char getSelectedSymbol()
    {
      return m_SymbolSet[getSelectedId()];
    }

    CButton* m_Enter;           // Button used for confirming and printing selected character
    CButton* m_Backspace;       // Button used as a backspace
    CButton* m_Left;            // Button for moving cursor left
    CButton* m_Right;           // Button for moving cursor right
    CText* m_Text;              // Text that should be manipulated with this CWriter
    char* m_SymbolSet;          // Set of available characters to input
    uint16_t m_SymbolSetLenght; 
    uint16_t m_CursorPos;       // Position of cursor
    CText m_SymbolsText;        // CText used for rendering selection
};

// Struct for manipulating program's memory
struct CMemory
{
  char m_TextBuffer[MEMORY_NUM_OF_BYTES * 4];
  int8_t m_SelectedId; // Index of currently selected byte
  
  CMemory()
    : m_Text(m_TextBuffer, MEMORY_NUM_OF_BYTES * 4 - 1, 24, MEMORY_X, MEMORY_Y),
      m_SelectedId(0)
  {}

  // Sets all bytes to zero and selects the first one
  void init()
  {
    m_SelectedId = 0;
    for (byte i = 0; i < MEMORY_NUM_OF_BYTES; i++)
    {
      setValAt(0, i);
    }
  }

  // Increments selected byte
  void incrementSelected()
  {
    byte value = getSelectedVal();
    value++;
    setSelectedVal(value);
  }

  // Decrements selected byte
  void decrementSelected()
  {
    byte value = getSelectedVal();
    value--;
    setSelectedVal(value);
  }

  // Un-selects byte and selects the one to it's left
  void moveSelectionLeft()
  {
    deselectAt(m_SelectedId--);
    if (m_SelectedId < 0)
      m_SelectedId = MEMORY_NUM_OF_BYTES - 1;
    selectAt(m_SelectedId);
  }

  // Un-selects byte and selects the one to it's right
  void moveSelectionRight()
  {
    deselectAt(m_SelectedId++);
    if (m_SelectedId >= MEMORY_NUM_OF_BYTES)
      m_SelectedId = 0;
    selectAt(m_SelectedId);
  }

  // Returns value of byte at m_SelectedId position
  byte getSelectedVal()
  {
    return getValAt(m_SelectedId);
  }

  // Returns value of byte at index 'id'
  byte getValAt(byte id)
  {
    byte idInText = id * 4;
    byte result = 0;
    sscanf(m_TextBuffer + idInText, "%03d", &result);
    return result;
  }

  // Sets value of byte at index m_SelectedId
  void setSelectedVal(byte value)
  {
    setValAt(value, m_SelectedId);
  }

  // Sets value of byte at index 'id'
  void setValAt(byte value, byte id)
  {
    byte idInText = id * 4;
    snprintf(m_TextBuffer + idInText, 4, "%03d ", value);
    if (id == m_SelectedId)
      selectAt(id);
    else
      deselectAt(id);
  }

  // Deselects byte at index 'id'
  void deselectAt(byte id)
  {
    id *= 4;
    m_Text.normalAt(id);
    m_Text.normalAt(id + 1);
    m_Text.normalAt(id + 2);
  }

  // Selects byte at index 'id'
  void selectAt(byte id)
  {
    id *= 4;
    m_Text.highlightAt(id);
    m_Text.highlightAt(id + 1);
    m_Text.highlightAt(id + 2);
  }

  // Text used for rendering bytes' values
  CText m_Text;
};

// Struct that implements stack data structure
// Used for storing program counter values to match brackets in the executed code
struct CStack
{
  uint16_t m_Buffer[STACK_SIZE];
  int16_t m_PointerId;

  CStack()
    : m_PointerId(0)
  {}

  bool push(uint16_t item)
  {
    if (isFull())
      return false;

    m_Buffer[++m_PointerId] = item;
    return true;
  }

  bool pop(uint16_t* itemOut)
  {
    if (isEmpty())
      return false;

    *itemOut = m_Buffer[m_PointerId--];
    return true;
  }

  bool isFull()
  {
    return (m_PointerId >= STACK_SIZE);
  }
  bool isEmpty()
  {
    return (m_PointerId <= 0);
  }
};

// Buttons on Esplora
CButton buttonR(CButton::RIGHT);
CButton buttonL(CButton::LEFT);
CButton buttonU(CButton::UP);
CButton buttonD(CButton::DOWN);
CButton buttonY(CButton::JOY);
CButton buttonYL(CButton::JOY_LEFT);
CButton buttonYR(CButton::JOY_RIGHT);

// Updates all buttons' internal states
void updateButtons()
{
  buttonR.update();
  buttonL.update();
  buttonU.update();
  buttonD.update();
  buttonY.update();
  buttonYL.update();
  buttonYR.update();
}

// Text for showing message (errors, hints, editor state, ...)
char messageBuffer[MESSAGE_MAX_LEN + 1];
CText message(messageBuffer, MESSAGE_MAX_LEN, MESSAGE_MAX_LEN + 1, MESSAGE_X, MESSAGE_Y, CColor(BLACK), CColor(BLACK));

// Text representing code inputed by the user
char codeBuffer[CODE_LEN + 1];
CText code(codeBuffer, CODE_LEN, CODE_LINE_LEN, CODE_X, CODE_Y);
CWriter codeInput(&buttonR, &buttonL, &buttonYL, &buttonYR, SYMBOLS_CODE, sizeof(SYMBOLS_CODE) - 1, &code);

// Text representing input for executed program
char inputBuffer[INPUT_LEN + 1];
CText inputText(inputBuffer, INPUT_LEN, INPUT_LINE_LEN, INPUT_X, INPUT_Y);
CWriter inputInput(&buttonR, &buttonL, &buttonYL, &buttonYR, SYMBOLS_DIGITS, sizeof(SYMBOLS_DIGITS) - 1, &inputText);

// Text representing output of executed program
char outputBuffer[OUTPUT_LEN + 1];
CText outputText(outputBuffer, OUTPUT_LEN, OUTPUT_LINE_LEN, OUTPUT_X, OUTPUT_Y);

CStack stack;
CMemory memory;

void displayMessage(char* str)
{
  memset(message.m_Text, 0, MESSAGE_MAX_LEN - 1);
  memcpy(message.m_Text, str, strlen(str));
  message.updateText();
}

void showMessage(char* str)
{
  message.m_Bg = CColor(GREEN);
  displayMessage(str);
}

void showError(char* str)
{
  message.m_Bg = CColor(RED);
  displayMessage(str);
}

void setup()
{
  Serial.begin(9600);

  screen.begin();
  screen.background(BLACK);

  screen.fill(CODE_COLOR_FG);
  screen.stroke(CODE_COLOR_FG);
  screen.line(0, MESSAGE_Y + CHAR_HEIGHT + 1, screen.width(), MESSAGE_Y + CHAR_HEIGHT + 1);
  delay(100);

  memory.init();
  outputText.updateText();

  inputInput.init();
  codeInput.init();
  inputInput.defocus();
  codeInput.defocus();
}

// When entering program's input, 
// this function changes available characters, that can be inputed
void loopThroughInputSets(int dir)
{
  static int8_t setId = 0;
  setId += dir;
  if (setId > 2) setId = 0;
  else if (setId < 0) setId = 2;

  switch (setId)
  {
    case 0:
      inputInput.changeSymbolSet(SYMBOLS_DIGITS, sizeof(SYMBOLS_DIGITS) - 1);
      break;
    case 1:
      inputInput.changeSymbolSet(SYMBOLS_ALPHABET_1, sizeof(SYMBOLS_ALPHABET_1) - 1);
      break;
    case 2:
      inputInput.changeSymbolSet(SYMBOLS_ALPHABET_2, sizeof(SYMBOLS_ALPHABET_2) - 1);
      break;
  }
}

// Returns how long the delay between executed instructions should be
// based on Esplora's slider
uint32_t getCodeDelay()
{
  return map(Esplora.readSlider(), 1024, 0, 0, 2000);
}

// Returns false if the code has unmatched brackets (therefore can't be executed properly)
bool checkCode()
{
  int bracketCnt = 0;
  int len = strlen(code.m_Text);
  for (int i = 0; i < len; i++)
  {
    char codeInstruction = code.m_Text[i];
    if (codeInstruction == '[') bracketCnt++;
    else if (codeInstruction == ']') bracketCnt--;

    if (bracketCnt >= STACK_SIZE) return false;
    if (bracketCnt < 0) return false;
  }
  return bracketCnt == 0;
}

// Clears values in program's memory
// and visually deselects all user-input methods
void setupCode()
{
  showMessage("Setting things up...");
  inputText.resetRead();
  outputText.resetAppend();
  inputInput.defocus();
  codeInput.defocus();
  memory.init();
  outputText.reset();
  delay(500);
}

// Runs written code
void runCode(bool asNumber)
{
  setupCode();
  showMessage(!asNumber ? "Running..." : "Running [num output]");

  uint16_t instructionId = 0;
  uint16_t prevInstructionId = 0;
  uint16_t codeLenght = code.getLength();
  char instruction;
  bool asNumberMode = asNumber;

  // Loop through all insctructions in code and execute them
  while (instructionId < codeLenght)
  {
    updateButtons();
    if (buttonY.isPressed()) // Mostly for exiting infinite loop programs
      break;

    // Highlight executed instruction
    code.normalAt(prevInstructionId);
    code.highlightAt(instructionId);

    delay(getCodeDelay() / 2);
    instruction = codeBuffer[instructionId];
    uint16_t newInstructionId = instructionId + 1;

    // Execute instruction
    switch (instruction)
    {
      case '+':
        memory.incrementSelected();
        break;
      case '-':
        memory.decrementSelected();
        break;
      case '<':
        memory.moveSelectionLeft();
        break;
      case '>':
        memory.moveSelectionRight();
        break;
      case '[':
        if (memory.getSelectedVal() == 0)
        {
          while (codeBuffer[newInstructionId++] != ']');
          break;
        }
        if (!stack.push(instructionId)) return;
        break;
      case ']':
        if (!stack.pop(&newInstructionId)) return;
        break;
      case '.':
        outputText.append(memory.getSelectedVal(), asNumberMode);
        break;
      case ',':
        memory.setSelectedVal(inputText.read(asNumberMode));
        break;
      default:
        break;
    }

    delay(getCodeDelay() / 2);

    prevInstructionId = instructionId;
    instructionId = newInstructionId;
  }
  code.normalAt(instructionId);
  code.normalAt(prevInstructionId);
}

// Page for coding
void codePage()
{
  showMessage("Code your program");
  codeInput.focus();
  while (!buttonY.isPressed())
  {
    updateButtons();
    codeInput.update();
  }
  while (buttonY.isPressed())
  {
    updateButtons();
  }
  codeInput.defocus();
}

// Page for entering inputs for program
void inputPage()
{
  showMessage("Enter program's inputs");
  inputInput.focus();
  while (!buttonY.isPressed())
  {
    updateButtons();
    inputInput.update();
    if (buttonD.isPressed())
    {
      loopThroughInputSets(1);
    }
    else if (buttonU.isPressed())
    {
      loopThroughInputSets(-1);
    }
  }

  while (buttonY.isPressed())
  {
    updateButtons();
  }
  inputInput.defocus();
}

// Page for detecting errors in code
// and running the program
void runPage()
{
  if (!checkCode())
  {
    showError("Unmatched brackets");
    while (!buttonY.isPressed())
    {
      updateButtons();
    }
  }
  else
  {
    bool messageWasShown = false;
    while (!buttonY.isPressed())
    {
      updateButtons();
      if (!messageWasShown)
      {
        showMessage("Left/Right to run");
        messageWasShown = true;
      }
      if (buttonR.isPressed())
      {
        runCode(false);
        messageWasShown = false;
      }
      if (buttonL.isPressed())
      {
        runCode(true);
        messageWasShown = false;
      }
    }
  }

  while (buttonY.isPressed())
  {
    updateButtons();
  }
}

void loop()
{
  codePage();
  inputPage();
  runPage();
}



